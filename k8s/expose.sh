#!/bin/bash
SCRIPT_PATH=$(dirname "${BASH_SOURCE[0]}")
# command to expose the deployment object
kubectl expose --type=LoadBalancer -f ${SCRIPT_PATH}/defs/deployment.yml