# Running a NodeJS app using Kubectl

This sample application includes scripts and examples for working in a Docker Kubernetes development environment.

## Setup

- Install the Kubernetes component, included in [Docker for Desktop](https://www.docker.com/products/docker-desktop) `v18.09.x` or later.
- If you are on a Mac OS, ensure that you select the `docker-for-desktop` context, if you happen to have minikube installed on your local machine.
