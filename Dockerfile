FROM node:8

RUN apt update
RUN apt install -y apt-transport-https tree 

ENV WORKDIR /opt/app

COPY . ${WORKDIR}

WORKDIR ${WORKDIR}

RUN tree -L 2

RUN npm run build

EXPOSE 8080

ENTRYPOINT [ "npm", "run", "start" ]