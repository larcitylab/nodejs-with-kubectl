import * as express from "express";
// import * as Http from "http";
// import { RequestHandler, Express } from "express";

const app = express.default();

const handleRequest: express.RequestHandler = (req, res) => {
  console.log(`Received request for URL: ${req.url}`);
  res.status(200).send("Hello World");
};

app.use("/", handleRequest);

const PORT = process.env.PORT || "8080";

// const www = Http.createServer(handleRequest);
app.listen(PORT, () => console.log(`Simple server listening @ ${PORT}`));
